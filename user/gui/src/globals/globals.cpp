#include <gui/globals/globals.h>
#include <cstring>
#include "mbed.h"

namespace 
{
	static uint32_t m_betrag = 0;
    static char tokenBuf[30] = {0};
}

namespace Global
{
	uint32_t betrag()
	{
		return m_betrag;
	}
	
	void setBetrag(uint32_t betrag)
	{
        m_betrag = betrag;
    }

    const char *token()
    {
        return tokenBuf;
    }

    void setToken(const char *tok)
    {
        CriticalSectionLock lock;
        strcpy(tokenBuf, tok);
    }

    bool tokenIsSet()
    {
        CriticalSectionLock lock;
        return strlen(tokenBuf) > 0;
    }

    void clearToken()
    {
        CriticalSectionLock lock;
        tokenBuf[0] = 0;
    }


}
