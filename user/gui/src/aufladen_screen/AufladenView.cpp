#include <gui/aufladen_screen/AufladenView.hpp>
#include <gui/globals/globals.h>



AufladenView::AufladenView() : m_euro(0), m_cent(0), m_sum(0)
{

}

void AufladenView::setupScreen()
{
    AufladenViewBase::setupScreen();
}

void AufladenView::tearDownScreen()
{
    AufladenViewBase::tearDownScreen();
}

void AufladenView::okayClicked()
{
	Global::setBetrag(Global::betrag() + m_sum);
}

 void AufladenView::sliderEuroChanged(int value)
 {
    m_euro = value;
	m_sum = m_euro * 100 + m_cent;
	
	Unicode::snprintf(taSumBuffer, TASUM_SIZE, "%d.%02d", m_sum / 100, m_sum % 100);
	taSum.invalidate();
	
	Unicode::snprintf(taEuroBuffer, TAEURO_SIZE, "%d", value);
	taEuro.invalidate();
 }

 void AufladenView::sliderCentChanged(int value)
 {
    m_cent = value;
	m_sum = m_euro * 100 + m_cent;
	 
	Unicode::snprintf(taSumBuffer, TASUM_SIZE, "%d.%02d", m_sum / 100, m_sum % 100);
	taSum.invalidate();
	 
	Unicode::snprintf(taCentBuffer, TACENT_SIZE, "%d", value);
	taCent.invalidate();
 }