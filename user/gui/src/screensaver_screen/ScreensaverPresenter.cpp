#include <gui/screensaver_screen/ScreensaverView.hpp>
#include <gui/screensaver_screen/ScreensaverPresenter.hpp>

ScreensaverPresenter::ScreensaverPresenter(ScreensaverView& v)
    : view(v)
{
}

void ScreensaverPresenter::activate()
{

}

void ScreensaverPresenter::deactivate()
{

}

void ScreensaverPresenter::tokenDetected()
{
    view.tokenDetected();
}
