#include <gui/screensaver_screen/ScreensaverView.hpp>
#include "hardware.h"
#include "globals.h"

ScreensaverView::ScreensaverView()
{

}

void ScreensaverView::setupScreen()
{
    ScreensaverViewBase::setupScreen();

    if (Global::tokenIsSet())
    {
        HW::writeBetragToSD();
        Global::clearToken();
    }
}

void ScreensaverView::tearDownScreen()
{
    ScreensaverViewBase::tearDownScreen();
    HW::readBetragFromSD();
}

void ScreensaverView::tokenDetected()
{
    application().gotoHauptmenuScreenCoverTransitionNorth();
}
