#include <gui/hauptmenu_screen/HauptmenuView.hpp>
#include <gui/globals/globals.h>
#include "mbed.h"
#include <FATFileSystem.h>
#include <SDIOBlockDevice.h>

static const uint32_t BETRAG = 70;


HauptmenuView::HauptmenuView()
{

}




void HauptmenuView::setupScreen()
{
    HauptmenuViewBase::setupScreen();
	updateBetrag();
}

void HauptmenuView::tearDownScreen()
{
    HauptmenuViewBase::tearDownScreen();
}

void HauptmenuView::updateBetrag()
{
	Unicode::snprintf(taGuthabenBuffer, TAGUTHABEN_SIZE, "%d.%02d", Global::betrag() / 100, Global::betrag() % 100);
	taGuthaben.invalidate();
	
	touchgfx::Image * images[] = {&image1,&image2,&image3,&image4,&image5,&image6,&image7,&image8,&image9,&image10,&image11,&image12,&image13};
	
	int numBottles = Global::betrag() / BETRAG;
	
	for (int i = numBottles; i < 13; i++)
	{
		images[i]->setAlpha(0);
	}
	
}

void HauptmenuView::entnahmeClicked()
{
	if (Global::betrag() >= BETRAG)
	{
		Global::setBetrag(Global::betrag() - BETRAG);
		updateBetrag();
        application().gotoErfolgreichScreenSlideTransitionEast();
	}
    else
    {
        application().gotoErfolglosScreenSlideTransitionEast();
    }
}

