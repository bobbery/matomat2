#ifndef ERFOLGLOS_VIEW_HPP
#define ERFOLGLOS_VIEW_HPP

#include <gui_generated/erfolglos_screen/ErfolglosViewBase.hpp>
#include <gui/erfolglos_screen/ErfolglosPresenter.hpp>

class ErfolglosView : public ErfolglosViewBase
{
public:
    ErfolglosView();
    virtual ~ErfolglosView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // ERFOLGLOS_VIEW_HPP
