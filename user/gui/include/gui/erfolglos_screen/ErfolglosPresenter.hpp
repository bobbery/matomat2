#ifndef ERFOLGLOS_PRESENTER_HPP
#define ERFOLGLOS_PRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class ErfolglosView;

class ErfolglosPresenter : public Presenter, public ModelListener
{
public:
    ErfolglosPresenter(ErfolglosView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~ErfolglosPresenter() {};

private:
    ErfolglosPresenter();

    ErfolglosView& view;
};


#endif // ERFOLGLOS_PRESENTER_HPP
