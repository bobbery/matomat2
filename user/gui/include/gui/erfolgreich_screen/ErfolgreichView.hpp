#ifndef ERFOLGREICH_VIEW_HPP
#define ERFOLGREICH_VIEW_HPP

#include <gui_generated/erfolgreich_screen/ErfolgreichViewBase.hpp>
#include <gui/erfolgreich_screen/ErfolgreichPresenter.hpp>

class ErfolgreichView : public ErfolgreichViewBase
{
public:
    ErfolgreichView();
    virtual ~ErfolgreichView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // ERFOLGREICH_VIEW_HPP
