#ifndef AUFLADEN_VIEW_HPP
#define AUFLADEN_VIEW_HPP

#include <gui_generated/aufladen_screen/AufladenViewBase.hpp>
#include <gui/aufladen_screen/AufladenPresenter.hpp>

class AufladenView : public AufladenViewBase
{
public:
    AufladenView();
    virtual ~AufladenView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
	
	virtual void sliderEuroChanged(int value);
    virtual void sliderCentChanged(int value);
	
	virtual void okayClicked();
	
private:

	int m_euro, m_cent, m_sum;
};

#endif // AUFLADEN_VIEW_HPP
