#ifndef GLOBALS_H
#define GLOBALS_H

#include <stdint.h>


namespace Global
{
	uint32_t betrag();
	void setBetrag(uint32_t betrag);

    const char* token();
    void setToken(const char* tok);
    bool tokenIsSet();
    void clearToken();
}

#endif
