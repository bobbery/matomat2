#ifndef HAUPTMENU_VIEW_HPP
#define HAUPTMENU_VIEW_HPP

#include <gui_generated/hauptmenu_screen/HauptmenuViewBase.hpp>
#include <gui/hauptmenu_screen/HauptmenuPresenter.hpp>

class HauptmenuView : public HauptmenuViewBase
{
public:
    HauptmenuView();
    virtual ~HauptmenuView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
	
	virtual void entnahmeClicked();

protected:

	void updateBetrag();

};

#endif // HAUPTMENU_VIEW_HPP
