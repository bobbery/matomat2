#ifndef SCREENSAVER_PRESENTER_HPP
#define SCREENSAVER_PRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class ScreensaverView;

class ScreensaverPresenter : public Presenter, public ModelListener
{
public:
    ScreensaverPresenter(ScreensaverView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual void tokenDetected();

    virtual ~ScreensaverPresenter() {};

private:
    ScreensaverPresenter();

    ScreensaverView& view;
};


#endif // SCREENSAVER_PRESENTER_HPP
