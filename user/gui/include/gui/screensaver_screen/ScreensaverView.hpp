#ifndef SCREENSAVER_VIEW_HPP
#define SCREENSAVER_VIEW_HPP

#include <gui_generated/screensaver_screen/ScreensaverViewBase.hpp>
#include <gui/screensaver_screen/ScreensaverPresenter.hpp>

class ScreensaverView : public ScreensaverViewBase
{
public:
    ScreensaverView();
    virtual ~ScreensaverView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
    virtual void tokenDetected();
protected:
};

#endif // SCREENSAVER_VIEW_HPP
