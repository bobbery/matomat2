#include <stdint.h>
#include <touchgfx/Unicode.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif



// Language Gb: No substitution
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId1_Gb[9] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x41, 0x75, 0x66, 0x6c, 0x61, 0x64, 0x65, 0x6e, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId2_Gb[5] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x4f, 0x6b, 0x61, 0x79, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId6_Gb[15] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x47, 0x75, 0x74, 0x68, 0x61, 0x62, 0x65, 0x6e, 0x3a, 0x20, 0x20, 0x2, 0x20, 0x20ac, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId7_Gb[13] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x41, 0x75, 0x66, 0x6c, 0x61, 0x64, 0x65, 0x6e, 0x20, 0x2, 0x20, 0x20ac, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId8_Gb[10] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x41, 0x62, 0x62, 0x72, 0x65, 0x63, 0x68, 0x65, 0x6e, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId10_Gb[6] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x2, 0x20, 0x63, 0x74, 0xa, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId14_Gb[9] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x45, 0x6e, 0x74, 0x6e, 0x61, 0x68, 0x6d, 0x65, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId15_Gb[5] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x30, 0x2e, 0x30, 0x30, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId16_Gb[21] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x45, 0x6e, 0x74, 0x6e, 0x61, 0x68, 0x6d, 0x65, 0xa, 0x65, 0x72, 0x66, 0x6f, 0x6c, 0x67, 0x72, 0x65, 0x69, 0x63, 0x68, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId20_Gb[24] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x45, 0x6e, 0x74, 0x6e, 0x61, 0x68, 0x6d, 0x65, 0x20, 0xa, 0x6e, 0x69, 0x63, 0x68, 0x74, 0x20, 0x6d, 0xf6, 0x67, 0x6c, 0x69, 0x63, 0x68, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId21_Gb[7] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x5a, 0x75, 0x72, 0xfc, 0x63, 0x6b, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId22_Gb[5] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x45, 0x6e, 0x64, 0x65, 0x0 };

TEXT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::Unicode::UnicodeChar* const textsGb[19] TEXT_LOCATION_FLASH_ATTRIBUTE =
{
    T_SingleUseId1_Gb,
    T_SingleUseId2_Gb,
    T_SingleUseId6_Gb,
    T_SingleUseId7_Gb,
    T_SingleUseId8_Gb,
    T_SingleUseId6_Gb+11,
    T_SingleUseId10_Gb,
    T_SingleUseId15_Gb+3,
    T_SingleUseId15_Gb+3,
    T_SingleUseId15_Gb,
    T_SingleUseId14_Gb,
    T_SingleUseId15_Gb,
    T_SingleUseId16_Gb,
    T_SingleUseId22_Gb,
    T_SingleUseId21_Gb,
    T_SingleUseId22_Gb,
    T_SingleUseId20_Gb,
    T_SingleUseId21_Gb,
    T_SingleUseId22_Gb
};

