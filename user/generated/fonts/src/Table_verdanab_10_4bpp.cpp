
#include <touchgfx/Font.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif

FONT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::GlyphNode glyphs_verdanab_10_4bpp[] FONT_LOCATION_FLASH_ATTRIBUTE =
{
    {     0,  63,   6,   8,   8,   0,   6, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0}
};

