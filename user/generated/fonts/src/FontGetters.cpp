// Autogenerated, do not edit

#include <touchgfx/InternalFlashFont.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif

// verdanab_10_4bpp
extern const touchgfx::GlyphNode glyphs_verdanab_10_4bpp[];
extern const uint8_t unicodes_verdanab_10_4bpp[];
extern const touchgfx::KerningNode kerning_verdanab_10_4bpp[];
touchgfx::InternalFlashFont& getFont_verdanab_10_4bpp();

touchgfx::InternalFlashFont& getFont_verdanab_10_4bpp()
{
    static touchgfx::InternalFlashFont verdanab_10_4bpp(glyphs_verdanab_10_4bpp, 1, 10, 0, 4, 0, 0, unicodes_verdanab_10_4bpp, kerning_verdanab_10_4bpp, 63, 0);
    return verdanab_10_4bpp;
}

// verdanab_20_4bpp
extern const touchgfx::GlyphNode glyphs_verdanab_20_4bpp[];
extern const uint8_t unicodes_verdanab_20_4bpp[];
extern const touchgfx::KerningNode kerning_verdanab_20_4bpp[];
touchgfx::InternalFlashFont& getFont_verdanab_20_4bpp();

touchgfx::InternalFlashFont& getFont_verdanab_20_4bpp()
{
    static touchgfx::InternalFlashFont verdanab_20_4bpp(glyphs_verdanab_20_4bpp, 40, 20, 4, 4, 0, 1, unicodes_verdanab_20_4bpp, kerning_verdanab_20_4bpp, 63, 0);
    return verdanab_20_4bpp;
}

// verdanab_40_4bpp
extern const touchgfx::GlyphNode glyphs_verdanab_40_4bpp[];
extern const uint8_t unicodes_verdanab_40_4bpp[];
extern const touchgfx::KerningNode kerning_verdanab_40_4bpp[];
touchgfx::InternalFlashFont& getFont_verdanab_40_4bpp();

touchgfx::InternalFlashFont& getFont_verdanab_40_4bpp()
{
    static touchgfx::InternalFlashFont verdanab_40_4bpp(glyphs_verdanab_40_4bpp, 1, 40, 0, 4, 0, 0, unicodes_verdanab_40_4bpp, kerning_verdanab_40_4bpp, 63, 0);
    return verdanab_40_4bpp;
}

