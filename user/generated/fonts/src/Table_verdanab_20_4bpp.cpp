
#include <touchgfx/Font.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif

FONT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::GlyphNode glyphs_verdanab_20_4bpp[] FONT_LOCATION_FLASH_ATTRIBUTE =
{
    {     0,  32,   0,   0,   0,   0,   7, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {     0,  46,   5,   4,   4,   1,   7, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {    12,  48,  13,  14,  14,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   110,  49,  11,  14,  14,   2,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   194,  50,  13,  14,  14,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   292,  51,  12,  14,  14,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   376,  52,  14,  14,  14,   0,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   474,  53,  12,  14,  14,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   558,  54,  13,  14,  14,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   656,  55,  12,  14,  14,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   740,  56,  14,  14,  14,   0,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   838,  57,  12,  14,  14,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   922,  58,   4,  11,  11,   2,   8, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   944,  63,  11,  14,  14,   1,  12, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1028,  65,  16,  14,  14,   0,  16, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1140,  69,  12,  14,  14,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1224,  71,  14,  14,  14,   1,  16, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1322,  79,  15,  14,  14,   1,  17, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1434,  90,  14,  14,  14,   0,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1532,  97,  12,  11,  11,   0,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1598,  98,  13,  15,  15,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1703,  99,  12,  11,  11,   0,  12, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1769, 100,  13,  15,  15,   0,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1874, 101,  13,  11,  11,   0,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1951, 102,   9,  15,  15,   0,   8, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2026, 103,  13,  15,  11,   0,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2131, 104,  12,  15,  15,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2221, 105,   5,  15,  15,   1,   7, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2266, 107,  13,  15,  15,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2371, 108,   5,  15,  15,   1,   7, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2416, 109,  19,  11,  11,   1,  21, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2526, 110,  12,  11,  11,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2592, 111,  13,  11,  11,   0,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2669, 114,   9,  11,  11,   1,  10, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2724, 116,   9,  14,  14,   0,   9, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2794, 117,  12,  11,  11,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2860, 121,  13,  15,  11,   0,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2965, 246,  13,  15,  15,   0,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  3070, 252,  12,  15,  15,   1,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  3160,8364,  14,  14,  14,   0,  14, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0}
};

