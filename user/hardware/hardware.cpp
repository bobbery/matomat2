#include "hardware.h"
#include "SDIOBlockDevice.h"
#include "FATFileSystem.h"
#include "globals.h"

#include <string>

namespace HW
{

void writeBetragToSD()
{
    SDIOBlockDevice &sd = SDIOBlockDevice::getInstance();
    FATFileSystem fs("sdcard");

    if (fs.mount(&sd) != 0)
    {
        printf("Fehler beim mounten\n");
    }
    else
    {
        std::string fName = "/sdcard/" + std::string(Global::token()) + ".txt";
        FILE *fp = fopen(fName.c_str(), "w");

        if (fp == NULL)
        {
            printf("Kann Datei nicht anlegen\n");
        }
        else
        {
            fprintf(fp,"%d", Global::betrag());
            fclose(fp);
            printf("Geschrieben: %d\n", Global::betrag());
        }
        fs.unmount();
    }
}

void readBetragFromSD()
{
    SDIOBlockDevice &sd = SDIOBlockDevice::getInstance();
    FATFileSystem fs("sdcard");

    if (fs.mount(&sd) != 0)
    {
        printf("Fehler beim mounten\n");
    }
    else
    {
        std::string fName = "/sdcard/" + std::string(Global::token()) + ".txt";
        FILE *fp = fopen(fName.c_str(), "r");

        if (fp == NULL)
        {
            printf("Datei wird angelegt\n");

            fp = fopen(fName.c_str(), "w");
            if (fp != NULL)
            {
                fprintf(fp, "%d", 0);
                Global::setBetrag(0);
                fclose(fp);
            }
        }
        else
        {
            int betrag = 0;
            fscanf(fp,"%d", &betrag);
            printf("Gelesen: %d\n", betrag);
            fclose(fp);

            Global::setBetrag(betrag);
        }
        fs.unmount();
    }
}




}
