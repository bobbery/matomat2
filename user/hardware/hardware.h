#ifndef HARDWARE_H
#define HARDWARE_H

#include "mbed.h"

namespace HW
{
    void writeBetragToSD();
    void readBetragFromSD();
}

#endif
