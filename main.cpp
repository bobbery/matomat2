#include "mbed.h"
#include "globals.h"
#include "MFRC522.h"

#include <touchgfx/hal/HAL.hpp>
#include <touchgfx/hal/BoardConfiguration.hpp>
#include <string>

#define SPI5_MOSI    A2
#define SPI5_MISO    A3
#define SPI5_SCK    A4
#define SPI5_CS      A5
#define MF_RESET    D10

using namespace touchgfx;

Thread gfxThread(osPriorityNormal1, 32768, NULL, "GUI Thread");
MFRC522 RfChip(SPI5_MOSI, SPI5_MISO, SPI5_SCK, SPI5_CS, MF_RESET);


FileHandle *mbed_override_console(int)
{
    static UARTSerial console(USBTX, USBRX);
    return &console;
}


void gui_thread()
{
    touchgfx::HAL::getInstance()->taskEntry();
}

std::string getToken()
{
    std::string uid;

    for (uint8_t i = 0; i < RfChip.uid.size; i++)
    {
        char buf[3];
        sprintf(buf, "%X02", RfChip.uid.uidByte[i]);
        uid.append(buf);
    }

    return uid;
}

int main()
{
    hw_init();
    touchgfx_init();
    RfChip.PCD_Init();

    gfxThread.start(gui_thread);

    while (true)
    {
        if (!Global::tokenIsSet())
        {
            if ( ! RfChip.PICC_IsNewCardPresent())
            {
                wait_ms(50);
                continue;
            }

            if ( ! RfChip.PICC_ReadCardSerial())
            {
                wait_ms(50);
                continue;
            }

            Global::setToken(getToken().c_str());
            printf("Token %s\r\n", getToken().c_str());
        }

        wait_ms(300);
    }
}

